import static org.junit.Assert.*;

import org.junit.Test;

public class CircleTest {

	// 1. Create a Junit file  - DONE
		// 2. Write test case in Junit file 
		//		-- EXPECTED OUTPUT
		// 		-- ACTUAL OUTPUT
		// 3. Run yout test case
		// 4. See what passed & failed
		
		// @Test -> 
		// Tells Junit to run the function as a test case
		// & report back if the test case passed or failed
		@Test
		public void testConstructor() {
			circle c = new circle(5);
			
			
			
			
			//	assertEquals(x,y,0.01);
			// x = expected output
			// y = actual output
			// 0.01 = just use 0.01
			// OPTION 1: WRite it like this:
			assertEquals(5, c.getRadius(), 0.01);	
			//assertEquals(expectedRadius, actualRadius, 0.01);		
		}
				
		@Test
		public void testAreaFunction() {
			circle c = new circle(5);
			double expected=Math.PI*Math.pow(5, 2);;
			assertEquals(expected, c.getArea(), 0.1);
		}
		
		//Test
		@Test
		public void testCircumFunction() {
			circle c = new circle(5);
			double expected=Math.PI*2*5;
			assertEquals(expected, c.getCircumference(), 0.01);
		}
		
		
		
		@Test
		public void testDiameterFunction() {
			circle c = new circle(5);
			double expected=Math.PI*5;
			assertEquals(expected, c.getDiameter(), 0.01);
		}

}
