
public class circle {
//properties
double radius;	
	
//Constructors
public circle(double r)
{
	this.radius=r;
	}

//Getters & Setters

public double getRadius() {
	return radius;
}

public void setRadius(double radius) {
	this.radius = radius;
}
	
//Custom Methods

public double getArea() {
	double area=Math.PI*Math.pow(radius, 2);
	System.out.println("Area"+area);
	return area;
}
	
public double getCircumference() {
double c = Math.PI*2*this.radius;
System.out.println("Circumference"+c);
return c;
}
public double getDiameter() {
	double diameter=Math.PI*this.radius;
	System.out.println("Diameter"+diameter);
	return diameter;
}
}
