
public class classMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		circle c = new circle(5);
		
		//R1:
		double expectedRadius = 5;
		double actualRadius = c.getRadius();
		if (expectedRadius == actualRadius) {
			System.out.println("R1: Working!");
		}
		else {
			System.out.println("R1: NOT Working!");
			System.out.println("Expected Result: " + expectedRadius);
			System.out.println("Actual Result: " + actualRadius);
		}
		
		//R2
		double expectedArea = 78.5;
		double actualArea = c.getArea();
		if (expectedArea == actualArea) {
			System.out.println("R2: Working!");
		}
		else {
			System.out.println("R2: NOT Working!");
			System.out.println("Expected Result: " + expectedArea);
			System.out.println("Actual Result: " + actualArea);
		}
		
		//R3
		double expectedCircumference = Math.PI*2*5;
		double actualCircumference = c.getCircumference();
		if (expectedCircumference == actualCircumference) {
			System.out.println("R3: Working!");
		}
		else {
			System.out.println("R3: NOT Working!");
			System.out.println("Expected Result: " + expectedCircumference);
			System.out.println("Actual Result: " + actualCircumference);
		}
		
		//R4
		double expectedDiameter = Math.PI*5;
		double actualDiameter = c.getDiameter();
		if (expectedDiameter == actualDiameter) {
			System.out.println("R4: Working!");
		}
		else {
			System.out.println("R4: NOT Working!");
			System.out.println("Expected Result: " + actualDiameter);
			System.out.println("Actual Result: " + actualDiameter);
		}
		
		
		
		// PROCESS FOR TESTING CODE
				// ----------------------------
				// 1. Read the requirements!
				// 2. Decide what to test!
						// R1:  Constructor sets radius
						// R2:  Areas calculates properly
						// R3:  Circufum calcuates properly
						// R4:  Diamemter calculates properly
				// 3. Make test cases
				//		// Test case = the thing you are trying to test
						// Working?
						// Expected Result  --> What SHOULD happen
						// Actual Result	--> What ACTUALLY happened
				
						// Test case:
						//		-- Working   (expected == actual)
						//		-- Not work	 (expected != actual)
				
				// 3.
				// 4.
				// 5. Debug
				// 6. 
	}

}
